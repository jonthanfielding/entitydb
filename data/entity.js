var db = null;
var _ = require('lodash');
var Q = require('q');

//
//Saving of entities
//TODO - Move these into a db write file
//

var getMetaTypes = function() {
    return db.select('id', 'name').from('meta-types');
};

var getEntityTypes = function() {
    return db.select('id', 'name').from('entity-types');
};

var saveEntity = function(entityTypes, typeName) {
    var type = _.where(entityTypes, { name: typeName })[0];
    
    return db('entity').insert({
        entityType: type.id
    });
};

var updateEntity = function(entityTypes, id, typeName) {
    var type = _.where(entityTypes, { name: typeName })[0];
    
    return db('entity').where('id', id).update({
        entityType: type.id
    });
};

var deleteEntity = function(id) {
    var deferred = Q.defer();

    db('entity').where('id', id).update({
        deleted: 1
    }).then(function(response) {
        deferred.resolve(response);
    });

    return deferred.promise;
};

var restoreEntity = function(id) {
    var deferred = Q.defer();

    db('entity').where('id', id).update({
        deleted: 0
    }).then(function(response) {
        deferred.resolve(response);
    });

    return deferred.promise;
};

var createEntitySlug = function() {
    
};

var saveMetaQuery = function(entityId, metaTypeId, value){
    var deferred = Q.defer();

    db('meta').insert({
        entityId: entityId,
        metaTypeId: metaTypeId,
        value: value
    }).then(function(response){
        deferred.resolve(response);
    });

    return deferred.promise;
}

var saveMetaData = function(metaTypes, metaData, entityId) {
    var metaKeys = _.keys(metaData);
    var metaId = null;
    var metaQueries = [];

    for (var i = 0; i < metaKeys.length; i++) {
        metaId = _.where(metaTypes, { name: metaKeys[i] })[0].id;

        metaQueries.push(saveMetaQuery( entityId, metaId,  metaData[metaKeys[i]]));
    }
    
    return Q.all(metaQueries);
};

//
//Read of entities
//TODO - Move these into a db read file
//

var makeEntityQuery = function(options) {
    var query = null;
    
    if (options.like) {
        query = db.select('entity.id', 'entity-types.name as type').from('meta').leftJoin('entity', 'entity.id', 'meta.entityId').leftJoin('entity-types', 'entity-types.id', 'entity.entityType').groupBy('entity.id');
        
        if (options.type) {
            query.where('entity-types.name', options.type);
        }
        
        if (options.like.column) {
            query.leftJoin('meta-types', 'meta-types.id', 'meta.metaTypeId').where('meta-types.name', options.like.column)
        }
        
        //Add case insensitive like query
        query.whereRaw('meta.value LIKE ? COLLATE utf8_general_ci', [options.like.query])
    }
    else if (_.isPlainObject(options.where)) {
        query = db.select('entity.id', 'entity-types.name as type').from('meta').leftJoin('entity', 'entity.id', 'meta.entityId').leftJoin('entity-types', 'entity-types.id', 'entity.entityType').groupBy('entity.id');

        if (options.type) {
            query.where('entity-types.name', options.type);
        }

        if (options.where.column) {
            query.leftJoin('meta-types', 'meta-types.id', 'meta.metaTypeId').where('meta-types.name', options.where.column)
        }

        //Add case insensitive equals query
        query.whereRaw('meta.value = ?', [options.where.query])
        
    }
    else if(_.isArray(options.where)) {
        //THIS DOES NOT WORK, PLEASE DO NOT USE

        console.error("ERROR: USING ARRAY FOR WHERE IS CURRENTLY BROKEN");

        query = db.select('entity.id', 'entity-types.name as type').from('meta').leftJoin('entity', 'entity.id', 'meta.entityId').leftJoin('entity-types', 'entity-types.id', 'entity.entityType').groupBy('entity.id');

        if (options.type) {
            query.where('entity-types.name', options.type);
        }

        query.leftJoin('meta-types', 'meta-types.id', 'meta.metaTypeId');

        for (var i = 0; i < options.where.length; i++) {
            var where = options.where[i];

            if (where.column) {
                query.where('meta-types.name', where.column)
            }

            //Add case insensitive equals query
            query.whereRaw('meta.value = ?', [where.query])
        }
    }
    else {
        query = db.select('entity.id', 'slug.path', 'entity-types.name as type').from('entity').leftJoin('slug', 'slug.entityId', 'entity.id').leftJoin('entity-types', 'entity-types.id', 'entity.entityType');

        if (options.type) {
            query.where('entity-types.name', options.type)
        }

        if (options.id) {
            query.where('entity.id', options.id);
        }
    }

    //Check whether the query wants deleted results or not
    if (options.deleted) {
        if (options.deleted === false) {
            query.where('entity.deleted', 0);
        }
        else if (options.deleted === "only") {
            query.where('entity.deleted', 1)
        }
    }
    else {
        query.where('entity.deleted', 0)
    }

    return query;
};

var getEntityById = function(id) {
    var deferred = Q.defer();
    
    makeEntityQuery({
        id: id
    }).then(function(response){
        deferred.resolve(response);
    });

    return deferred.promise;
};

var getEntityByType = function(type, opts) {
    var deferred = Q.defer();
    var options = opts || {};
    var query = makeEntityQuery(_.extend(options, {type:type}));

    query.then(function(response){
        if(response.length) {
            response.map(function(item){
                if (item.path === null) {
                    item.path = "";
                }

                return item;
            });

            deferred.resolve(response);
        }
        else {
            deferred.reject(response);
        }
    });

    return deferred.promise;
};

var getEntityBySlug = function(path) {
    var deferred = Q.defer();
    var query = db.select('entity.id', 'slug.path', 'entity-types.name as type').from('entity').leftJoin('slug', 'slug.entityId', 'entity.id').leftJoin('entity-types', 'entity-types.id', 'entity.entityType').where('slug.path', path);

    query.then(function(response){

        if(response.length) {
            response.map(function(item){
                if (item.path === null) {
                    item.path = "";
                }

                return item;
            });

            deferred.resolve(response);
        }
        else {
            deferred.reject(response);
        }
        
            
    });

    return deferred.promise;
};

var getRelatedMeta = function(response) {
    var resolve = [];

    for (var i = 0; i < response.length; i++) {
        resolve.push(queryRelatedMeta(response[i]));
    }

    return Q.all(resolve);
};

var queryRelatedMeta = function(entity) {
    var deferred = Q.defer();

    db.select('meta-types.name', 'meta.value').from('entity').leftJoin('meta', 'meta.entityId', 'entity.id').leftJoin('meta-types', 'meta.metaTypeId', 'meta-types.id').where('entity.id', entity.id).then(function(response){
        var meta = {};
        var buffer;

        for (var i = 0; i < response.length; i++) {
            if(response[i].value !== null){
                buffer = new Buffer(response[i].value);
                meta[response[i].name] = buffer.toString();
            }
        }

        entity.meta = meta;

        deferred.resolve(entity);
    }, function(){
        deferred.reject(entity);
    });

    return deferred.promise;
};

var searchEntities = function(opts) {
    var deferred = Q.defer();
    var options = opts || {};
    var query = makeEntityQuery(options);

    query.then(function(response){
        response.map(function(item){
            if (item.path === null) {
                item.path = "";
            }

            return item;
        });

        deferred.resolve(response);
    });

    return deferred.promise;
};


module.exports = function (connection) {
    db = connection;
    
    return {
        new: function(data) {
          return Q(getEntityTypes()
              .then(_.partialRight(saveEntity, data.type))
              .then(function(returned){
                  var entityId = returned[0];
                  
                  if(data.meta){
                      return getMetaTypes().then(_.partialRight(saveMetaData, data.meta, entityId)).then(function() {
                          return getEntityById(entityId).then(getRelatedMeta);
                      });
                  }
                  else {
                      return returned;
                  }
              }));
        },
        update: function(type, data) {
            return getEntityTypes()
                .then(_.partialRight(updateEntity, type, data));
        },
        delete: function(id) {
            return deleteEntity(id);
        },
        restore: function(id) {
            return restoreEntity(id);
        },
        getEntityById: function(id) {
          return getEntityById(id).then(getRelatedMeta);
        },
        getEntityByType: function(type, options) {
          console.log("Get Entity By Type: " + type + ' Options: ' + JSON.stringify(options));
          return getEntityByType(type, options).then(getRelatedMeta);
        },
        getEntityBySlug: function(path) {
          return getEntityBySlug(path).then(getRelatedMeta);
        },
        searchEntities: function(options) {
            return searchEntities(options).then(getRelatedMeta);
        }
    };
};
