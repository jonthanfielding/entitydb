var db = null;
var _ = require('lodash');
var Q = require('q');
var user = null;

//
//Saving of tokens
//TODO - Move these into a db write file
//

var guid = (function() {
    function s4() {
        return Math.floor((1 + Math.random()) * 0x10000)
            .toString(16)
            .substring(1);
    }
    return function() {
        return s4() + s4() + '-' + s4() + '-' + s4() + '-' +
            s4() + '-' + s4() + s4() + s4();
    };
})();

var saveToken = function(userId, token) {
    return db('access').insert({
        userId: userId,
        guid: token
    }).then(function(response){
        if (response.length) {
            return token;
        }
    });
};

//
//Read of tokens
//TODO - Move these into a db read file
//

var retrieveUserId = function(token) {
    var deferred = Q.defer();

    db.select('*').from('access').where('guid', token).then(function(response){
        deferred.resolve(response);
    });

    return deferred.promise;
};

var retrieveUserDetails = function(token) {
    var deferred = Q.defer();

    db.select('*').from('access').leftJoin('user', 'access.userId', 'user.id').where('access.guid', token).then(function(response){
        deferred.resolve(response);
    }, function(error){
        deferred.reject(error);
    });

    return deferred.promise;
};

module.exports = function(connection) {
    db = connection;
    user = require('./user')(db);
    
    return {
        createToken: function(data) {
            var token = guid();
            return saveToken(data.userId, token);
        },
        getUserId: function(token) {
            return retrieveUserId(token).then(function(response){
                if (response.length) {
                    return response[0].userId;
                }
                else {
                    return false;
                }
            });
        },
        getUserDetails: function(token) {
            return retrieveUserDetails(token).then(user.getUserMeta).then(function(response){
                if (response.length) {
                    return response[0];
                }
                else {
                    return false;
                }
            });
        }
    };
};



