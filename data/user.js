var _ = require('lodash');
var Q = require('q');
var credential = require('credential');

var getMetaTypes = function() {
    return db.select('id', 'name').from('user-meta-types');
};

var getMetaTypeByName = function(name) {
    var deferred = Q.defer();
    
    db.select('id').from('user-meta-types').where('name', name).then(function(response){
        if (response.length) {
            deferred.resolve(response[0].id);
        }
        else {
            deferred.reject(false);
        }
    });

    return deferred.promise;
};

var hashPassword = function (password) {
    var deferred = Q.defer();

    credential.hash(password, function (err, hash) {
        if (err) {
            deferred.reject(err);
        }
        else {
            deferred.resolve(hash);
        }
    });

    return deferred.promise;
};

var verifyUserCredentials = function (userData, enteredPassword) {
    var deferred = Q.defer();
    var user;

    if (userData.length) {
        user = userData[0];

        credential.verify(user.password, enteredPassword, function (err, isValid) {
            if (err) {
                deferred.reject(false);
            }

            if (isValid) {
                delete user.password;
                deferred.resolve(user);
            }
            else {
                deferred.reject("Invalid username and password combination.");
            }
        });
    }
    else {
        deferred.reject(false);
    }

    return deferred.promise;
};

//
//Saving of user
//TODO - Move these into a db write file
//

var makeUserQuery = function(options) {
    var query = null;
};

var saveUser = function(passwordHash, data) {
    return Q(db('user').insert({
        username: data.username,
        password: passwordHash,
        name: data.name
    }));
};

var saveMetaData = function(metaTypes, metaData, userId) {
    var metaKeys = _.keys(metaData);
    var metaId = null;

    for (var i = 0; i < metaKeys.length; i++) {
        metaId = _.where(metaTypes, { name: metaKeys[i] })[0].id;

        db('user-meta').insert({
            userId: userId,
            metaTypeId: metaId,
            value: metaData[metaKeys[i]]
        }).then(function(response){

        });
    }
};

var getUserById = function(id) {
    var deferred = Q.defer();

    db.select('*').from('user').where('id', id).then(function(response) {
        deferred.resolve(response[0]);
    }, deferred.reject);

    return deferred.promise;
};

var getUserByUsername = function(username) {
    var deferred = Q.defer();

    db.select('*').from('user').where('username', username).then(function(response){
        if(response.length){
            deferred.resolve(response);
        }
        else {
            deferred.reject(false);
        }
        
    });

    return deferred.promise;
};

var getUserIdByMeta = function(id, value) {
    var deferred = Q.defer();

    var query = db.select('*').from('user-meta').where('metaTypeId', id).where('value', value).then(function(response){
        deferred.resolve(response);
    }, function(response){
        deferred.reject(response);
    });

    return deferred.promise;
};

var getUserMeta = function(response) {
    var resolve = [];

    if (_.isArray(response)){
        for (var i = 0; i < response.length; i++) {
            resolve.push(queryRelatedMeta(response[i]));
        }
        
        return Q.all(resolve);
    }
    else {
        return queryRelatedMeta(response);
    }
};

var getUsers = function(options) {
    var deferred = Q.defer();
    var query = db.select('id', 'username').from('user');

    //Check whether the query wants deleted results or not
    if (options.deleted) {
        if (options.deleted === false) {
            query.where('user.deleted', 0);
        }
        else if (options.deleted === "only") {
            query.where('user.deleted', 1)
        }
    }
    else {
        query.where('user.deleted', 0)
    }
    
    
    query.then(function(response){
        deferred.resolve(removeNull(response));
    });

    return deferred.promise;
};

var removeNull = function(response) {
    return _.map(response, function(item){
        return _.pick(item, _.identity);
    });
}

var queryRelatedMeta = function(user) {
    var deferred = Q.defer();

    db.select('user-meta-types.name', 'user-meta.value').from('user-meta').leftJoin('user-meta-types', 'user-meta.metaTypeId', 'user-meta-types.id').where('user-meta.userId', user.id).then(function(response){
        var meta = {};
        var buffer;

        for (var i = 0; i < response.length; i++) {
            if(response[i].value !== null){
                buffer = new Buffer(response[i].value);
                meta[response[i].name] = buffer.toString();
            }
        }

        user.meta = meta;

        deferred.resolve(user);
    }, function(error){
        deferred.reject(error);
    });

    return deferred.promise;
};

var deleteUser = function(id) {
    var deferred = Q.defer();

    db('user').where('id', id).update({
        deleted: 1
    }).then(function(response) {
        deferred.resolve(response);
    });

    return deferred.promise;
};

var restoreUser = function(id) {
    var deferred = Q.defer();

    db('user').where('id', id).update({
        deleted: 0
    }).then(function(response) {
        deferred.resolve(response);
    });

    return deferred.promise;
};

var updateUser = function(id, values) {
    var deferred = Q.defer();
    var updatedValues = {};
    
    if (values.name) {
        updatedValues.name = values.name;
    }

    if (values.password) {
        hashPassword(values.password).then(function(passwordHash){
            updatedValues.password = passwordHash;

            if(_.isEmpty(updatedValues) === false) {
                db('user').where('id', id).update(updatedValues).then(function (response) {
                    deferred.resolve(response);
                });
            }
        })
    }
    else if(_.isEmpty(updatedValues) === false) {
        db('user').where('id', id).update(updatedValues).then(function(response){
            deferred.resolve(response);
        });
    }
    
    return updateUserMeta(id, values.meta);
};

function updateUserMeta (id, meta) {
    var promises = [];
    var keys = _.keys(meta);
    var fetchedMetaTypes = getMetaTypes();

    for (var i = 0; i < keys.length; i++) {
        promises.push(function(key){
            var deferred = Q.defer();

            fetchedMetaTypes.then(function(metaTypes) {
                var metaTypeId = _.where(metaTypes, { name: key })[0].id
                var sql = db('user-meta').where('userId', id).where('metaTypeId', metaTypeId);

                sql.then(function(selectResponse){
                    if (selectResponse.length) {
                        var query = db('user-meta').where('userId', id).where('metaTypeId', metaTypeId).update({
                            value: meta[key]
                        });
                    }
                    else {
                        var query = db('user-meta').insert({
                            userId: id,
                            metaTypeId: metaTypeId,
                            value: meta[key]
                        });
                    }

                    query.then(function(response){
                        deferred.resolve(response);
                    }, function(error){
                        deferred.reject(error);
                    });
                });


            });

            return deferred.promise;
        }(keys[i]));
    }

    return Q.all(promises);
}

module.exports = function (connection) {
    db = connection;

    return {
        new: function (data) {
            return hashPassword(data.password).then(_.partialRight(saveUser, data)).then(function (returned) {
                var userId = returned[0];

                if (data.meta) {
                    getMetaTypes().then(_.partialRight(saveMetaData, data.meta, userId));
                }
                
                return getUserById(userId);
            });
        },
        getUsers: function(options) {
            return getUsers({}).then(getUserMeta);
        },
        getUserById: function (id) {
            return getUserById(id).then(getUserMeta);
        },
        getUserByUsername: function (username) {
            return getUserByUsername(username.toLowerCase()).then(getUserMeta);
        },
        getUserByMeta: function(key, value) {
            var deferred = Q.defer();
            
            getMetaTypeByName(key).then(_.partialRight(getUserIdByMeta, value)).then(function(response){
                var usersToFetch = [];
                
                if (!response.length) {
                    deferred.reject(0);
                }
                
                for (var i = 0; i < response.length; i++) {
                    usersToFetch.push(getUserById(response[i].userId).then(getUserMeta));
                }
                
                Q.all(usersToFetch).then(function(result){
                    deferred.resolve(result);
                });
            });
            
            return deferred.promise;
        },
        validateUser: function (data) {
            return getUserByUsername(data.username).then(getUserMeta).then(_.partialRight(verifyUserCredentials, data.password));
        },
        getUserMeta: getUserMeta,
        delete: function(id) {
            return deleteUser(id);
        },
        restore: function(id) {
            return restoreUser(id);
        },
        update: function(id, values) {
            return updateUser(id, values);
        }
    };
};
