var db = null;
var _ = require('lodash');
var Q = require('q');

//
//Saving of entities
//TODO - Move these into a db write file
//

var saveSlug = function(data) {
    return db('slug').insert({
        path: data.path,
        entityId: data.entityId
    });
};

//
//Read of slugs
//TODO - Move these into a db read file
//

var getSlugById = function(id) {
    var deferred = Q.defer();

    db.select('*').from('slug').where('id', id).then(function(response){
        deferred.resolve(response);
    });

    return deferred.promise;
};

var getSlugByEntityId = function(id) {
    var deferred = Q.defer();

    db.select('*').from('slug').where('entityId', id).then(function(response){
        deferred.resolve(response);
    });

    return deferred.promise;
};

module.exports = function (connection) {
    db = connection;

    return {
        new: function(data) {
            return saveSlug(data);
        },
        getSlugById: function(id) {
            return getSlugById(id);
        },
        getSlugByEntityId: function(id) {
            return getSlugByEntityId(id);
        }
    };
};

