var knex = require('knex');

module.exports = function(dbDetails) {
    var db = knex(dbDetails);
    
    //test connection
    db.raw('select 1+1 as result').then(function () {
        console.log('Connection to database working correctly');
    });
    
    return {
        entity: require("./data/entity")(db),
        access: require("./data/access")(db),
        slug: require("./data/slug")(db),
        user: require("./data/user")(db)
    };
};
